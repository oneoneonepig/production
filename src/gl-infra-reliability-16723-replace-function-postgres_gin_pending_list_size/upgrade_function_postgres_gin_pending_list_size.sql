/*
Issue: https://gitlab.com/gitlab-com/gl-infra/reliability/-/issues/16723

To apply, run this SQL script using gitlab-psql.

This change:
* Skips indexes that are being created or dropped, to avoid throwing an error for a non-existent metapage 0.
* Cosmetic: For clarity, use INNER instead of LEFT OUTER joins, and rename table aliases.
*/

BEGIN
;

/*
Because this is a set-returning function with an associated pg_type for its result rows, we must
explicitly DROP FUNCTION to implicitly remove its old type, rather than just CREATE OR REPLACE FUNCTION.
*/

DROP FUNCTION public.postgres_gin_pending_list_size()
;

/*
This updated version of the function includes the following changes:
  * Filter to indislive and indisready to both be true, so we skip indexes being dropped or created.
  * Use INNER JOINs.  There should never be pg_index rows without a match in pg_class and pg_am.

Implementation notes:
  * amname = 'gin' -- Find indexes of type GIN.
  * relkind = 'i'  -- Match only physical indexes that have a metapage.  Skip virtual parent of partitioned indexes (I).
  * indislive      -- Skip an index that is being dropped.
  * indisready     -- Skip an index that is not yet maintained by INSERT/UPDATE (e.g. concurrent phase of index creation).
*/

CREATE OR REPLACE FUNCTION public.postgres_gin_pending_list_size()
  RETURNS TABLE(index_name text, pending_list_bytes bigint)
  LANGUAGE sql
  SECURITY DEFINER
AS $function$
  WITH gin_indexes AS (
    SELECT i.indexrelid
    FROM
      pg_index i
      JOIN pg_class c ON i.indexrelid = c.oid
      JOIN pg_am a ON c.relam = a.oid
    WHERE
      a.amname = 'gin'
      AND c.relkind = 'i'
      AND i.indislive
      AND i.indisready
  )
  SELECT
    indexrelid::regclass::text AS index_name,
    n_pending_pages * current_setting('block_size')::bigint AS pending_list_bytes
  FROM
    gin_indexes,
    gin_metapage_info(get_raw_page(indexrelid::regclass::text, 0))
$function$
;

/*
Function "postgres_gin_pending_list_size()" is declared as a security-definer function with its owner set to
a postgres superuser because it wraps "get_raw_page()", which can only be called by a superuser.
*/

ALTER FUNCTION public.postgres_gin_pending_list_size() OWNER TO "gitlab-superuser"
;

COMMIT
;
